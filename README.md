# About
A compiler that translates the input language to output language, see Rules.txt for the rules of those languages
# How to Run:
Run makefile then do:
./pl<filename

it will do the typechecking and output the results in the console

I tried it with:

Flex version  : 2.5.35

bison version 2.5

gcc version : 4.6.3

OS: ubuntu-12.04.5