%{
#include <stdio.h>
#include <string.h>
#include "Node.h"
#include  <assert.h>
#include "vector.h"
//I did not implemented the Vector I used an implementation I found online
void yyerror(const char *s)
{
	printf("%s\n", s);
}

//Vector
void vector_init(vector *v)
{
	v->data = NULL;
	v->size = 0;
	v->count = 0;
}
 
int vector_count(vector *v)
{
	return v->count;
}
 
void vector_add(vector *v, void *e)
{
	//printf("ADD START\n");
	if (v->size == 0) {
		v->size = 10;
		v->data = malloc(sizeof(void*) * v->size);
		//memset(v->data, '\0', sizeof(void) * v->size);
		memset(v->data, '\0', sizeof(void *) * v->size);
	}
 
	// condition to increase v->data:
	// last slot exhausted
	if (v->size == v->count) {
		v->size *= 2;
		v->data = realloc(v->data, sizeof(void*) * v->size);
	}
 
	v->data[v->count] = e;
	v->count++;
	//printf("ADD END\n");
}
 
void vector_set(vector *v, int index, void *e)
{
	if (index >= v->count) {
		return;
	}
 
	v->data[index] = e;
}
 
void *vector_get(vector *v, int index)
{
	if (index >= v->count) {
		return;
	}
 
	return v->data[index];
}
 
void vector_delete(vector *v, int index)
{
	if (index >= v->count) {
		return;
	}
 
	v->data[index] = NULL;
 
	int i, j;
	void **newarr = (void**)malloc(sizeof(void*) * v->count * 2);
	for (i = 0, j = 0; i < v->count; i++) {
		if (v->data[i] != NULL) {
			newarr[j] = v->data[i];
			j++;
		}		
	}
 
	free(v->data);
 
	v->data = newarr;
	v->count--;
}
 
void vector_free(vector *v)
{
	free(v->data);
}
 

vector types;
vector ids;
int error=0;
vector arrayLengths;
vector values;
char * getTypeOfId(char * id);
int indexOfVector(char * id , vector searchedV);
void printVector(vector vToPrint);
void copy_string(char *target, char *source);
Node * createNode(int value,int arrayLength,char * typeName,char *variableName);
void addToVector(int arrayLength,char * typename,char * name);
char * getOperationType(char * operand1,char* op,char *operand2);
void updateValue (char * id , int value);

%}
%union {
int line;
char *name;
int value;
Node *nodePtr;
}
%token <value>tINTNUM tREALNUM <name>tID tINT tREAL tBOOLEAN tBEGIN tEND <line>tIF tTHEN tELSE <line>tWHILE tFALSE tTRUE <line>tCOMMA tLPAR tRPAR <line>tSLPAR <line>tSRPAR tASSIGN
%type <nodePtr> Expr
%type <nodePtr> Lval
%type <nodePtr> Type
%type <nodePtr> BinOp
%type <nodePtr> UnOp
%left <line>tNOT
%left <line>tSUB
%left <line>tMULT
%left <line>tPLUS
%left <line>tSMALLER
%left <line>tEQ
%left <line>tAND
%% /*Grammar follows */
Prgrm: VarDeclLst StmtBlk {/*printVector(ids);printVector(types);*/}
;
VarDeclLst: VarDecl VarDeclLst
			|VarDecl {}
			;
VarDecl: Type tID tCOMMA 
{ 
  char * id = $2;
  char* typeName = $1->typeName;
  addToVector(-1,typeName,id);
}
|Type tID tSLPAR tINTNUM tSRPAR tCOMMA 
{
  int arrayLength = $4;
  char * typeName = $1->typeName;
  char * id = $2;
  addToVector(arrayLength,typeName,id);
		 
}
;
Type: tINT {$$=createNode(-1,-1,"integer","NA");}
	  |tREAL {$$=createNode(-1,-1,"real","NA");}
	  |tBOOLEAN {$$=createNode(-1,-1,"boolean","NA");}
	  ;
StmtBlk: tBEGIN StmtLst tEND
;
StmtLst: Stmt StmtLst
		 |Stmt
		 ;
Stmt : AsgnStmt 
	   |IfStmt
	   |WhlStmt
	   ;
AsgnStmt:Lval tASSIGN Expr tCOMMA
{
	int lineCount =$4;
	if(strcmp($1->typeName,$3->typeName)!=0)
	{
		error=1;
		printf("\nNear line %d At ASSGNSTMT Type mismatch '%s' and  '%s'" ,lineCount, $1->typeName,$3->typeName);
		
	}
	else
	{
		char * id = $1->variableName;
		updateValue(id,$3->value);
	}

}
;
Lval: tID 
{
	char * id = $1;
	char * type = getTypeOfId(id);
	$$= createNode(-1,-1,type,id);
			
}
|tID tSLPAR Expr tSRPAR
{
	char * id = $1;
	char * type = getTypeOfId(id);
	int arrayLength = getArrayLength(id);
	int expressionValue = $3->value;
	int lineCount= $2;
	char * expressionType = $3->typeName;
	
	if(strcmp(expressionType,"integer")!=0)
	{
		error=1;
		printf("\nNear Line % d: At LVAL  the index of array '%s' is not an integer it is %s",lineCount,id,expressionType);
	}
	
	
	if(arrayLength==-1)
	{
		error=1;
		printf("\nNear Line %d: AT LVAL  '%s' is not an array",lineCount, id);
		$$= createNode(-1,-1,type,id);
	}
	else if((expressionValue>arrayLength||expressionValue<1) && arrayLength!=-1)
	{
		
		error=1;
		if(expressionValue>arrayLength)
		{
			printf("\nNear Line %d: AT LVAL Array is out of range ( %d > %d)  ",lineCount,expressionValue,arrayLength);
		}
		else if (expressionValue<1)
		{
			printf("\nNear Line %d: AT LVAL Array is out of range ( %d < 1)  ",lineCount,expressionValue);
		}
		
		$$=createNode(-1,arrayLength,type,"NA");
	}
	else
	{
		$$=createNode(-1,arrayLength,type,"NA");
	}
	
	
	
	  	
}
;
IfStmt:tIF tLPAR Expr tRPAR tTHEN StmtBlk tELSE StmtBlk
{
  char * typeName = $3->typeName;
  int lineCount = $1;
  if(strcmp(typeName,"boolean")!=0)
  {
	 error=1;
	 printf("\nNear line %d: AT IFSTMT Expression on the IF should be boolean",lineCount);
  }
}
;
WhlStmt:tWHILE tLPAR Expr tRPAR StmtBlk
{
  char * typeName = $3->typeName;
  int lineCount = $1;
  if(strcmp(typeName,"boolean")!=0)
  {
	error=1;
	printf("\nNear Line %d: AT WHLSTMT Expression on the WHILE should be boolean",lineCount);
  }		
}
;
Expr: tINTNUM { $$=createNode($1,-1,"integer","NA");} 
	  |tREALNUM {$$=createNode(-1,-1,"real","NA");}
	  |tFALSE { $$=createNode(-1,-1,"boolean","NA");}	
	  |tTRUE  { $$=createNode(-1,-1,"boolean","NA");}	
	  |tID    
{
  char *typeName=getTypeOfId($1);
  char * idName = $1;
  int value=getValue(idName);
  
  $$=createNode(value,-1,typeName,idName);
}
|tID tSLPAR Expr tSRPAR 
{
	char * typeName = getTypeOfId($1);
	int arrayLength= getArrayLength($1);
	int lineCount=$2;
	if(arrayLength!=-1)
	{
	   int exprValue=$3->value;
	   if(exprValue>arrayLength|| exprValue<1)
	   {
	  	   if(exprValue>arrayLength)
	  	   {
	  	   	error=1;
	  	   	printf("\nNear Line %d : AT EXPR Array Index is out of range (%d > %d)",lineCount,exprValue,arrayLength);
	  	   }
	  	   else if (exprValue<1)
	  	   {
	  	   	error=1;
	  	   	printf("\nNear Line %d : AT EXPR Array Index is out of range (%d < 1)",lineCount,exprValue);
	  	   }
	  	   
	   }
	   $$=createNode(-1,arrayLength,typeName,"NA");
	  
	 }
	 else
	 {
	  	error=1;
	  	printf("\nNear Line %d : AT EXPR it is not an array",lineCount);
	  	$$=createNode(-1,-1,typeName,"NA");
	 }
}
	  |tLPAR Expr tRPAR
|Expr BinOp Expr 
{ 
    char * operand1typeName = $1->typeName;
    char * operand2typeName = $3->typeName;
    char * op = $2->typeName;
    int lineCount = $2->value;
    char * resultType = getOperationType(operand1typeName,op,operand2typeName);
    //printf ( "\nResult type returned is %s \n",resultType);
    if (strcmp(resultType,"NA")==0)
	{
		error=1;
		printf("\nNear Line %d : EXPR Type mismatch '%s' and '%s'" ,lineCount, $1->typeName, $3->typeName);
		$$ = createNode(-1,-1,"NA","NA");  					 		
	}
	else
	{
		int value =-7;
		if(strcmp(op,"PLUS")==0 && strcmp(resultType,"integer")==0)
		{
	      value = $1->value + $3->value;
	  	  $$=createNode(value,-1,resultType,"NA");
	 	}
	  	else if (strcmp(op,"MULT")==0 && strcmp(resultType,"integer")==0)
	  	{
	  	  value = $1->value * $3->value;
	  	  $$=createNode(value,-1,resultType,"NA");
	  	}
	  	else
	  	{
	  	  $$=createNode(-1,-1,resultType,"NA");
	  	}	 		  					 	    
	}
}
|UnOp Expr 
{ 
  char * operatorName = $1->typeName;
  char * expressionType = $2->typeName;
  int expressionValue =  $2->value;
  if(strcmp(operatorName,"SUB")==0)
  {
	 if(strcmp(expressionType,"integer")==0)
	 {
	    int newValue = -1 * expressionValue;
	  	$$ = createNode(newValue,-1,expressionType,"NA");	
	  }
	  else if (strcmp(expressionType,"real")==0)
	  {
	  	$$= createNode(-1,-1,expressionType,"NA");
	  }
	  		
   }
}
	  ;
BinOp: tPLUS {$$=createNode($1,-1,"PLUS","NA");}
	   |tMULT {$$=createNode($1,-1,"MULT","NA");}
	   |tSMALLER {$$=createNode($1,-1,"SMALLER","NA");}
	   |tEQ {$$=createNode($1,-1,"EQ","NA");}
	   |tAND {$$=createNode($1,-1,"AND","NA");}
	   ;
UnOp: tSUB {$$=createNode($1,-1,"SUB","NA");}
	  |tNOT{$$=createNode($1,-1,"NOT","NA");}
	  ;
%%		 
int main()
{	
	
	//printf("Version 52");
	printf("\n////STARTING CHECKING//////");
	vector_init(&types);
	vector_init(&ids);
	vector_init(&arrayLengths);
	vector_init(&values);
	if(yyparse())
	{
		printf("ERROR\n" );
		//return 1;
	}
	else
	{
		printf("\nGRAMMAR IS OK\n");
		if(error==0)
		{
			printf("\nNO ERRORS FOUND \n");
		}
		else
		{
			printf("\nTHERE WAS ERRORS(PRINTED ABOVE)\n");
		}
	}
	printf("\n///FINISHED CHECKING//\n");
	return 0;
}

char * getTypeOfId(char * id)
{
	
	int index = indexOfVector(id,ids);
	//printf("found in %d ",index );
	char * foundType=vector_get(&types,index);
	return foundType;
}
int getArrayLength(char *id)
{
	int index = indexOfVector(id,ids);
	//printf("found in %d ",index );
	int arrayLength=vector_get(&arrayLengths,index);
	return arrayLength;
}

int getValue(char *id)
{
	int index = indexOfVector(id,ids);
	//printf("found in %d ",index );
	int value=vector_get(&values,index);
	return value;
}

void updateValue (char * id , int value)
{
	int index = indexOfVector(id,ids);
	vector_set(&values,index,value);

}

void addToVector(int arrayLength,char * typename,char * name)
{
	//printf ( "Adding tuple Name : %s , TypeName: %s \n",name,typename);
	char *addName=strdup(name);
	char *addTypeName = strdup(typename);
	vector_add(&types,addTypeName);
	//printVector(types);
	vector_add(&values,-1);
	vector_add(&ids,addName);
	vector_add(&arrayLengths,arrayLength);
	//printVector(ids);
}



void printVector(vector vToPrint)
{
	printf ( "\nPrinting the vector: \n");	
	int i;	
	for(i=0;i<vector_count(&vToPrint);i++)
	{
		printf("[%d]=%s \n" ,i, vector_get(&vToPrint,i));	
	}
	printf("\n");
}

int indexOfVector(char * id , vector searchedV)
{
	//printf("Searching %s " ,id );
	int i;	
	for(i=0;i<vector_count(&searchedV);i++)
	{
		char * text=vector_get(&searchedV,i);
		if(strcmp(id,text)==0)
		{
			return i;		
		}		
			
	}
	return -1;
}

char * getOperationType(char * operand1,char* op,char *operand2)
{
	char * ret="NA";
	if(strcmp("PLUS",op)==0)
	{
		if(strcmp(operand1,"integer")==0 && strcmp(operand2,"integer")==0)
		{
			ret = "integer";
		}
		else if(strcmp(operand1,"real")==0 && strcmp(operand2,"integer")==0)
		{
			ret= "real";
		}
		
		else if(strcmp(operand1,"integer")==0 && strcmp(operand2,"real")==0)
		{
			ret= "real";
		}
		
		else if(strcmp(operand1,"real")==0 && strcmp(operand2,"real")==0)
		{
			ret = "real";
		}
	}
	else if(strcmp("MULT",op)==0)
	{
		if(strcmp(operand1,"integer")==0 && strcmp(operand2,"integer")==0)
		{
			ret = "integer";
		}
		else if(strcmp(operand1,"real")==0 && strcmp(operand2,"integer")==0)
		{
			ret= "real";
		}
		else if(strcmp(operand1,"integer")==0 && strcmp(operand2,"real")==0)
		{
			ret= "real";
		}
		
		else if(strcmp(operand1,"real")==0 && strcmp(operand2,"real")==0)
		{
			ret = "real";
		}
	}
	else if(strcmp("SMALLER",op)==0)
	{
		if(strcmp(operand1,"integer")==0 && strcmp(operand2,"integer")==0)
		{
			ret = "boolean";
		}
		else if(strcmp(operand1,"real")==0 && strcmp(operand2,"real")==0)
		{
			ret= "boolean";
		}
	
	}
	else if(strcmp("EQ",op)==0)
	{
		if(strcmp(operand1,"integer")==0 && strcmp(operand2,"integer")==0)
		{
			ret = "boolean";
		}
		else if(strcmp(operand1,"real")==0 && strcmp(operand2,"real")==0)
		{
			ret= "boolean";
		}
		else if(strcmp(operand1,"boolean")==0 && strcmp(operand2,"boolean")==0)
		{
			ret="boolean";
		}
		
	}
	else if(strcmp("AND",op)==0)
	{
		if(strcmp(operand1,"boolean")==0 && strcmp(operand2,"boolean")==0)
		{
			ret = "boolean";
		}
	}
	
	return ret;
}

Node * createNode(int value,int arrayLength,char * typeName,char *variableName)
{
	Node * ret = (Node *) malloc (sizeof(Node));
	ret->typeName = typeName;
	ret->value = value;
	ret->arrayLength=arrayLength;
	ret->variableName=variableName;
	return (ret);
}







